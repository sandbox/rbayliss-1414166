<?php

/**
 * @file
 * This is a compatibility library for the DateTime object.  Its purpose is to
 * make the behavior of DateTimeCompatible exactly the same, regardless of PHP
 * varsion (going back to 5.2 anyway.  There is some obvious overhead in using
 * this class rather than the core DateTime, but it is necessary at this point.
 *
 * If you end up needing to pass in a version argument, use one of the constants,
 * as the values of these may change in the future.
 *
 * If and when you use type-hinting in your functions, you should always type
 * hint to the core objects rather than the compatible versions.  This will
 * allow for easy replacement of this compatibility layer when it becomes
 * uneccesary (PHP 5.4?).
 *
 * Bad:
 * @code
 *  function myfunction(DateTimeCompatible $date) {...}
 * @endcode
 *
 * Good:
 * @code
 *  function myfunction(DateTime $date) {...}
 * @endcode
 * 
 */


class DateTimeCompatible extends DateTime implements DateTimeCompatibleInterface {
  /**
   * @var The system compatibility level
   */
  static $defaultVersion;

  /**
   * @var the object's compatibility level.
   */
  var $version;
  
  /**
   * Detect the compatibility from the PHP version.
   */
  static function detectVersion() {
    if(version_compare(PHP_VERSION, '5.2', '<')) {
      return self::INCOMPATIBLE;
    }
    if(version_compare(PHP_VERSION, '5.3', '<')) {
      return self::PHP52;
    }
    if(version_compare(PHP_VERSION, '5.4', '<')) {
      return self::PHP53;
    }
    return self::PHP54;
  }

  /**
   * Return the static default version, detecting it if it is not set.
   */
  static function defaultVersion() {
    return isset(self::$defaultVersion) ? self::$defaultVersion : self::$defaultVersion = self::detectVersion();
  }

  /**
   * Overridden constructor.
   *
   * Creates a new DateTimeCompatible, with an optional version argument.
   * 
   * @param type $time
   * @param type $object
   * @param type $version
   */
  public function __construct($time, $object = NULL, $version = NULL) {
    $this->version = isset($version) ? $version : self::defaultVersion();
    parent::__construct($time, $object);
  }

  public static function createFromFormat($format, $time, DateTimeZone $object = NULL, $version = NULL) {
    if(self::defaultVersion() >= self::PHP53 || $version >= self::PHP53) {
      isset($object) ?
        $datetime = parent::createFromFormat($format, $time, $object) :
        $datetime = parent::createFromFormat($format, $time);

      return new self($datetime->format(DateTime::ISO8601), $datetime->getTimezone(), $version);
    }
    //@todo: this is a tricky one... can we do it?
    return new self($time, $object, $version);
  }

  public function add(DateInterval $interval) {
    if($this->version >= self::PHP53) {
      return parent::add($interval);
    }
    $op = $interval->invert ? '-' : '+';
    if($interval->y) {
      $this->modify("$op $interval->y years");
    }
    if($interval->m) {
      $this->modify("$op $interval->m months");
    }
    if($interval->d) {
      $this->modify("$op $interval->d days");
    }
    if($interval->h) {
      $this->modify("$op $interval->h hours");
    }
    if($interval->m) {
      $this->modify("$op $interval->m minutes");
    }
    if($interval->s) {
      $this->modify("$op $interval->s seconds");
    }
    return $this;
  }

  public function sub(DateInterval $interval) {
    if($this->version >= self::PHP53) {
      return parent::sub($interval);
    }
    $op = $interval->invert ? '+' : '-';
    if($interval->y) {
      $this->modify("$op $interval->y years");
    }
    if($interval->m) {
      $this->modify("$op $interval->m months");
    }
    if($interval->d) {
      $this->modify("$op $interval->d days");
    }
    if($interval->h) {
      $this->modify("$op $interval->h hours");
    }
    if($interval->m) {
      $this->modify("$op $interval->m minutes");
    }
    if($interval->s) {
      $this->modify("$op $interval->s seconds");
    }
    return $this;
  }

  public function diff(DateTime $object, $absolute = FALSE) {
    if($this->version >= self::PHP53) {
      //@todo: There may be bugs in the 5.3 implementation.
      //@todo: Should we try to return a compatible object?
      $dateInterval = parent::diff($object, $absolute);
    }
    //Create a new interval, we'll set the values manually.
    $diff = new DateIntervalCompatible('P0D', $this->version);
    if($this > $object) {
      $diff->invert = 1;
      $diff->y = $this->format('Y') - $object->format('Y');
      $diff->m = $this->format('m') - $object->format('m');
      $diff->d = $this->format('j') - $object->format('j');
      $diff->h = $this->format('G') - $object->format('G');
      $diff->i = $this->format('i') - $object->format('i');
      $diff->s = $this->format('s') - $object->format('s');
      //@todo: For some reason, we aren't able to set days here, at least not in
      //5.3.  Is is due to some sort of bad C interactions?
    }
    else {
      $diff->y = $object->format('Y') - $this->format('Y');
      $diff->m = $object->format('m') - $this->format('m');
      $diff->d = $object->format('j') - $this->format('j');
      $diff->h = $object->format('G') - $this->format('G');
      $diff->i = $object->format('i') - $this->format('i');
      $diff->s = $object->format('s') - $this->format('s');
      //@todo: For some reason, we aren't able to set days here, at least not in
      //5.3.  Is is due to some sort of bad C interactions?
    }

    $diff = normalize_diff($this, $diff);
    
    return $diff;
  }

  /**
   * Overridden modify().  
   */
  public function modify($modify) {
    if($this->version >= self::PHP54) {
      return parent::modify($modify);
    }
    if($this->version >= self::PHP53) {
      //@todo: I know there are some bugs in 5.3 modify.  Find out what they are
      //and how we can fix them.
      return parent::modify($modify);
    }
    //@todo: I know there are some bugs in 5.2 modify.  Find out what they are
    //and how we can fix them.
    return parent::modify($modify);
  }

  /**
   * Overridden setTimeStam().
   */
  public function setTimestamp($unixtimestamp) {
    if($this->version >= self::PHP53) {
      return parent::setTimestamp($unixtimestamp);
    }
    //@todo: Check this implementation.
    if($string = gmstrftime('%Y %m %d %H %M %S', $unixtimestamp)) {
      $parts = explode(' ', $string);
      $tmp_timezone = $this->getTimezone();
      $this->setTimezone(new DateTimeZone('UTC'));
      $this->setDate($parts[0], $parts[1], $parts[2]);
      $this->setTime($parts[3], $parts[4], $parts[5]);
      $this->setTimezone($tmp_timezone);
      return $this;
    }
    return FALSE;
  }
}

/**
 * Helper to normalize a DateInterval.
 *
 * This allows us to set the values of the interval to $a - $b and ignore the
 * fact that the values are negative... We go through and re-distribute them
 * here, moving minutes -> seconds, hours -> minutes, and so on.
 */
function normalize_diff(DateTime $dt, $diff) {
  $clone = clone $dt;
  while($diff->s < 0) {
    $diff->s += 60;
    $diff->i--;
  }
  while($diff->i < 0) {
    $diff->i += 60;
    $diff->h--;
  }
  while($diff->h < 0) {
    $diff->h += 24;
    $diff->d--;
  }
  while($diff->d < 0) {
    $clone->modify('-1 month');
    //@todo: Verify this works in all cases.
    $diff->d += $clone->format('t');
    $diff->m--;
  }
  while($diff->m < 0) {
    $diff->m += 12;
    $diff->y--;
  }
  return $diff;
}