DateTimeCompatible
==================

This is a compatibility library to bring the PHP DateTime object up to a standard level between versions 5.2, 5,3 and 5.4.  It aims to make using DateTime object consistent and bug-free for 5.2+.

Warning:
--------

As of this writing, the library is considered under development and is not ready for any sort of production use.  Use at your own risk.

Using it:
---------
1.  Make sure it is either included or autoloaded.
2.  Use the DateTimeCompatible object instead of the DateTime object anytime ou need to ensure compatiblity.
3.  Use any of the PHP 5.3 DateTime methods (including DateTime::add(), DateTime::sub(), DateTime::diff(), etc), and they should behave exactly the same as under PHP 5.4.

Notes:
----------
*  There is obviously a performance hit to using this user-space library instead of the native PHP objects.  If you don't need these bugfixes or PHP5.2 compatibility, use the DateTime object instead.  At the time of this writing, it is averaging just under 2x slower.

*  Submit bugs and patches.  Please.  While I'm not aiming to make this library fix *every* bug in PHP DateTime support, I'd like to fix anything there's an easy solution for.