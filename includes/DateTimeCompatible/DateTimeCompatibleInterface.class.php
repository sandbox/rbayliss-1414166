<?php

/**
 * This interface should be implemented by compatibility layers.
 */
interface DateTimeCompatibleInterface {
  const INCOMPATIBLE = 0;
  const PHP52 = 2;
  const PHP53 = 3;
  const PHP54 = 4;
}