<?php
require_once '../DateTimeCompatibleInterface.class.php';
require_once '../DateIntervalCompatible.class.php';
require_once '../DateTimeCompatible.class.php';

class DateTimeCompatibleTest extends PHPUnit_Framework_TestCase
{
  const DIFF_FORMAT = '%R %YY %MM %DD %HH %II %SS';
  var $version = DateTimeCompatible::PHP53; //You can change the version here if needed.
  //@todo: These tests are pretty simple.  Let's build this out.

    public function testCreation()
    {
      $datestrings = array(
        'now' => 'Now works properly.',
        'December' => 'A date with only a month works properly',
        'December 18' => 'A date with a month and day works properly',
        'December 18, 1992' => 'A date with month, day and year works properly.',
        'December 18, 1992 5:00' => 'A date with a time works properly',
        'December 18, 1992 5:00 PM' => 'A date with a time works properly',
      );
      foreach($datestrings as $dateval => $message) {
        $date = new DateTimeCompatible($dateval, NULL,  $this->version);
        $this->assertDateEquals($date, $dateval, $message);
      }
    }

    public function testSub() {
      $date = new DateTimeCompatible('January 1, 2000, 12:00 AM', NULL,  $this->version);

      $datestrings = array(
        'P1D' => array('d' => 'December 31, 1999', 'm' => 'Interval of 1 day was subtracted correctly.'),
        'P1Y1D' => array('d' => 'December 31, 1998', 'm' => 'Interval of 1 day 1 year was subtracted correctly.'),
      );

      foreach($datestrings as $interval => $data) {
        $newdate = clone $date;
        $newdate->sub(new DateInterval($interval));
        $this->assertDateEquals($newdate, $data['d'], $data['m']);
      }
    }

    public function testAdd() {
      $date = new DateTimeCompatible('January 1, 2000, 12:00 AM', NULL,  $this->version);

      $datestrings = array(
        'P1D' => array('d' => 'January 2, 2000', 'm' => 'Interval of 1 day was subtracted correctly.'),
        'P1Y1D' => array('d' => 'January 2, 2001', 'm' => 'Interval of 1 day 1 year was subtracted correctly.'),
      );

      foreach($datestrings as $interval => $data) {
        $newdate = clone $date;
        $newdate->add(new DateInterval($interval));
        $this->assertDateEquals($newdate, $data['d'], $data['m']);
      }
    }

    public function testDiff() {
      $compatible = new DateTimeCompatible('January 1, 2000, 12:00 AM', NULL,  $this->version);

      $datestrings = array(
        'January 2, 2000' => array('i' => '+ 00Y 00M 01D 00H 00I 00S', 'm' => 'Diff of +1 day.'),
        'December 31, 1999' => array('i' => '- 00Y 00M 01D 00H 00I 00S', 'm' => 'Diff of -1 day.'),
        'December 31, 2020' => array('i' => '+ 20Y 11M 30D 00H 00I 00S', 'm' => 'Diff of 20 years 1 day.'),
      );

      foreach($datestrings as $datestring => $data) {
        $newcompatible = new DateTimeCompatible($datestring, NULL, $this->version);

        $compatibleinterval = $compatible->diff($newcompatible);
        $this->assertEquals($compatibleinterval->format(self::DIFF_FORMAT), $data['i'], $data['m']);
      }
    }

    /**
     * Test the relative performance of using DateTimeCompatible over DateTime.
     *
     * @todo: This should probably be broken down by method so we can see where
     * things are slow.
     */
    public function testPerformance() {
      $iterations = 1000;
      foreach(array('DateTime', 'DateTimeCompatible') as $class) {
        $start = microtime();
        
        for($i = 0; $i < $iterations; $i++) {
          if($class == 'DateTimeCompatible') {
            $t = new $class('January 1, 2000, 12:00 AM', NULL, $this->version);
          }
          else {
            $t = new $class('January 1, 2000, 12:00 AM', NULL);
          }
          $t->sub(new DateIntervalCompatible('P1DT2S'));
          $t->add(new DateIntervalCompatible('P1DT2S'));
          $t->format(DateTime::ISO8601);
          $t->diff(new DateTime('January 1, 2000'));
        }
        $end = microtime();
        $totals[$class] = $end - $start;
      }
      print "\n\nRelative Performance Test for $iterations iterations:";
      foreach($totals as $class => $total) {
        $formatted_diff = round($total * 1000, 2);
        print "\n$class: $formatted_diff";
      }
    }

    public function assertDateEquals(DateTime $date, $dateString, $message = '', $format = DateTime::ISO8601) {
      $comparison_date = new DateTime($dateString);
      $this->assertEquals($date->format($format), $comparison_date->format($format), $message);
    }
}
?>
