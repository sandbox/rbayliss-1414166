<?php

/**
 * We have to declare the DateInterval class because it won't exist for PHP < 5.3
 */
if(!class_exists('DateInterval', FALSE)) {
  class DateInterval {
    public $y = 0;
    public $m = 0;
    public $d = 0;
    public $h = 0;
    public $i = 0;
    public $s = 0;
    public $invert = FALSE;
    public $days = -99999;

    public function __construct($interval_spec) {}
  }
}

class DateIntervalCompatible extends DateInterval implements DateTimeCompatibleInterface {
  /**
   * @var The system compatibility level
   */
  static $defaultVersion;

  /**
   * @var the object's compatibility level.
   */
  var $version;

  public $days = FALSE;

  /**
   * Detect the compatibility from the PHP version.
   */
  static function detectVersion() {
    if(version_compare(PHP_VERSION, '5.2', '<')) {
      return self::INCOMPATIBLE;
    }
    if(version_compare(PHP_VERSION, '5.3', '<')) {
      return self::PHP52;
    }
    if(version_compare(PHP_VERSION, '5.4', '<')) {
      return self::PHP53;
    }
    return self::PHP54;
  }

  /**
   * Return the static default version, detecting it if it is not set.
   */
  static function defaultVersion() {
    return isset(self::$defaultVersion) ? self::$defaultVersion : self::$defaultVersion = self::detectVersion();
  }

  /**
   * Overridden constructor.
   *
   * Creates a new DateTimeCompatible, with an optional version argument.
   *
   * @param type $time
   * @param type $object
   * @param type $version
   */

  public function __construct($interval_spec, $version = NULL) {
    $this->version = isset($version) ? $version : self::defaultVersion();
    
    if($this->version >= self::PHP53) {
      return parent::__construct($interval_spec);
    }
    
    //We have to initialize the parent in order to set values when testing 5.2 compatibility.
    parent::__construct('P0D');
    //Are we dealing with a P0001-00-04T00:00:00 format?
    //@todo: We need to verify that values do not exceed the period (http://php.net/manual/en/class.dateinterval.php)
    if(preg_match('/^P(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})$/', $interval_spec, $matches)) {
      list($date, $time) = explode('T', ltrim($interval_spec, 'P'));
      list($this->y, $this->m, $this->d) = array_map('intval', explode('-', $date));
      list($this->h, $this->i, $this->s) = array_map('intval', explode(':', $time));
      return $this;
    }
    //Are dealing with a format like P2YT4H
    //@todo: Make sure this regex will fail if we put date parts out of order.
    elseif(preg_match('/^P(\d+((Y|M|D))|(T\d+(H|I|S)))+/', $interval_spec)) {
      $regex = '/^P(?:(\d+)Y)?(?:(\d+)M)?(?:(\d+)D)?(?:T(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?)?/';
      if(preg_match($regex, $interval_spec, $matches) && !empty($matches)) {
        array_shift($matches);
        $keys = array('y', 'm', 'd', 'h', 'i', 's');
        $matches = array_filter($matches);
        $matches = $matches + array_fill(0, 6, 0);
        ksort($matches);
        $parts = array_combine($keys, $matches);
        foreach($parts as $key => $value) {
          $this->{$key} = intval($value);
        }
        return $this;
      }
    }
    throw new Exception(t('Unknown or bad format @string', array('@string' => $interval_spec)));
  }

  public function format($format) {
    if($this->version >= self::PHP53) {
      return parent::format($format);
    }
    $tokens = array(
      '%Y' => str_pad($this->y, 2, '0', STR_PAD_LEFT),
      '%y' => $this->y,
      '%M' => str_pad($this->m, 2, '0', STR_PAD_LEFT),
      '%m' => $this->m,
      '%D' => str_pad($this->d, 2, '0', STR_PAD_LEFT),
      '%d' => $this->d,
      //@see https://bugs.php.net/bug.php?id=49778
      '%a' => ($this->days == -99999) ? '(unknown)' : $this->days, 
      '%H' => str_pad($this->h, 2, '0', STR_PAD_LEFT),
      '%h' => $this->h,
      '%I' => str_pad($this->i, 2, '0', STR_PAD_LEFT),
      '%i' => $this->i,
      '%S' => str_pad($this->s, 2, '0', STR_PAD_LEFT),
      '%s' => $this->s,
      '%R' => ($this->invert) ? '-' : '+',
      '%r' => ($this->invert) ? '-' : '',
    );
    return strtr($format, $tokens);
  }
}